import time
import csv
import itertools
import operator # operator.itemgetter is supposedly faster than using lambda functions for sorting
import os
import sys
import Utility
import Node
import pickle
import Read
import gzip

ranks_lookup = ["forma", "varietas", "subspecies", "species", "species_subgroup", "species_group", "subgenus", "genus",
                                "infratribe", "subtribe", "tribe", "infrafamily", "subfamily", "family", "superfamily", "parvorder",
                                "infraorder", "suborder", "order", "superorder", "infraclass", "subclass", "class", "superclass",
                                "subphylum", "phylum", "superphylum", "subkingdom", "kingdom", "superkingdom", "no_rank"]

splits_family = [["bacteria", 2, "family"], ["viruses", 10239, "family"], ["apicomplexa", 5794, "family"], ["amoebozoa", 554915, "family"], ["fungi", 4751, "family"]]
splits_genus = [["bacteria", 2, "genus"], ["viruses", 10239, "genus"], ["apicomplexa", 5794, "genus"], ["amoebozoa", 554915, "genus"], ["fungi", 4751, "genus"]]
splits_species = [["bacteria", 2, "species"], ["viruses", 10239, "species"], ["apicomplexa", 5794, "species"], ["amoebozoa", 554915, "species"], ["fungi", 4751, "species"]]
splits_all = splits_family + splits_genus + splits_species

def quantify_annotations_in_smp_file_old(inputfile, outputdir, group_by="", splits=[], report_only_grouped=False, uniques_only=False, sort_by_unique_hits=False):
    """
    This method creates csv files containing the stats generated from a given smp file.
    It can split the output into several files via taxonomical groups (i.e. Bacteria and Viruses)

    inputfile - string path to input smp file
    outputdir - string path to output directory
    group_by - standard grouping parameter, e.g. "species", "genus", "family", is only used if the next parameter is left at default, default is an empty string, meaning no grouping
    splits - list containing groups to split output by - e.g. splits = [["bacteria", 2, "genus"], ["viridae", 10239, "family"]], where the number in each entry is the taxonomy id on ncbi, default is an empty list, leading to one single output file
    report_only_grouped - boolean defining whether to report only entries that could be grouped (some annotation strings don't include all levels of taxonomy which can lead to "ungroupable" entries), default is False
    uniques_only - boolean defining whether to report only reads that hit only one reference, default is False

    Return value is a list of produced files.
    """

    infile = open(inputfile)
    filesize = os.path.getsize(inputfile)
    bytesread = 0
    linesread = 0

    # if no splits parameter is given, we group by group_by if that is given and go on to produce a single output file
    if len(splits) == 0:

        outfile = open(outputdir+"/stats.csv", "w") # this is for the standard case ...
        csvwriter = csv.writer(outfile, dialect="excel", delimiter=";") # ... and will be overwritten if splits parameter is used
        output = {}

        #go through input file line wise and build a hashmap of this form { annotationstring: [ hits, unique hits ], nextstring: [ hits, uniquehits ] }
        for line in infile:
            linesread += 1
            bytesread += len(line)
            if linesread % 500 == 0:
                sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file\r")
                sys.stdout.flush()

            line_split = line.split()

            # get rid of the readname, sequence and qualities, leaving the first reference as line_split[0]
            for i in range(3):
                line_split.pop(0)

            # throw out the identifier that comes first in the annotation strings, so we can group stuff
            length_of_line_split = len(line_split)
            for index in range(0, length_of_line_split, 4): # go through line_split in steps of 4 iterating over the annotation strings
                temp_list = line_split[index].split("|")
                temp_list.pop(0) # get rid of the reference identifier number
                line_split[index] = "|".join(temp_list)

            # group even more if desired
            if group_by != "":
                length_of_line_split = len(line_split)
                for index in range(0, length_of_line_split, 4): # go through line_split in steps of 4 iterating over the annotation strings
                    if "|"+group_by in line_split[index]:
                        line_split[index] = line_split[index][line_split[index].index("|"+group_by)+1:] # cut off everything before the grouping parameter
                    else: # annotation strings without the grouping parameter
                        if not report_only_grouped: # leave them as they are if we report everything
                            continue
                        else: # delete them from the array completely if we only want the things containing the grouping parameter reported
                            line_split[index] = "" # string
                            line_split[index+1] = "" # hitlength
                            line_split[index+2] = "" # mismatches
                            line_split[index+3] = "" # clippings
                # if we removed anything we need to get rid of the empty items in the array now
                line_split = "\t".join(line_split).split()

            if len(line_split) == 0: # line without annotations
                continue
            elif len(line_split) == 4: # line with a unique annotation
                # look if annotation string in dictionary
                # if yes, add hits AND unique hits
                # if no, add whole entry
                if not output.has_key(line_split[0]):
                    output[line_split[0]] = [1, 1]
                else:
                    output[line_split[0]][0] += 1
                    output[line_split[0]][1] += 1

            else: # line with more than one annotation
                length_of_line_split = len(line_split)
                for index in range(0, length_of_line_split, 4): # go through line_split in steps of 4 iterating over the annotation string
                    if not output.has_key(line_split[index]):
                        output[line_split[index]] = [1, 0]
                    else:
                        output[line_split[index]][0] += 1

        # make an array out of the dictionary so we can sort it
        array = []
        for (key, value) in output.iteritems():
            array.append([key, value[0], value[1]])

        # once we are done with creating the array, we want to sort it after the species with the most hits
        sys.stdout.write("\nSorting\n")
        sys.stdout.flush()
        if sort_by_unique_hits:
            array.sort(key=operator.itemgetter(2,1), reverse=True) # two sorts actually: result is sorted by number of unique hits and then number of overall hits; highest first
        else:
            array.sort(key=operator.itemgetter(1,2), reverse=True) # two sorts actually: result is sorted by number of hits and then number of unique hits; highest first

        # if only uniques are to be reported, omit all entries without unique reads
        if uniques_only:
            array = [entry for entry in array if entry[2] > 0]

        # and then write it to the output file
        sys.stdout.write("Writing to output file\n")
        sys.stdout.flush()
        for entry in array:
            csvwriter.writerow(entry)

        # return list (with one entry) containing the path of the produced file
        return [os.path.abspath(outfile.name)]

    else: # splits parameter is given and group_by is ignored because it is given individually for each split group
        list_of_dicts = [{}] # should contain the dictionary created for each split group and one more dictionary into which we put all entries that don't fit any of the split groups

        # create a dictionary for each of the groups by which we want to split our results
        for split_group in splits: # i really need to come up with better names ... "split", "group", "split group" gets a bit confusing
            list_of_dicts += [{}]
        index_last_dict = len(list_of_dicts)-1

        # then go through the smp file line wise ... business as usual
        for line in infile:
            linesread += 1
            bytesread += len(line)
            if linesread % 500 == 0:
                sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file\r")
                sys.stdout.flush()

            line_split = line.split()

            # get rid of the readname, sequence and qualities, leaving the first reference as line_split[0]
            for i in range(3):
                line_split.pop(0)

            # throw out the identifier that comes first in the annotation strings, so we can group stuff
            length_of_line_split = len(line_split)
            for index in range(0, length_of_line_split, 4): # go through line_split in steps of 4 iterating over the annotation strings
                temp_list = line_split[index].split("|")
                temp_list.pop(0) # get rid of the reference identifier number
                line_split[index] = "|".join(temp_list)

            # now iterate over the references for the current read again and check which dictionary the hit has to be put into, after cutting everything we dont want in it (for grouping)
            # go through annotations for each read again
            for index in range(0, length_of_line_split, 4):
                # check which dictionary it has to be put into
                dictionary_index = index_last_dict
                for group_index, split_group in enumerate(splits):
                    if ":"+str(split_group[1])+":" in line_split[index]:
                        dictionary_index = group_index
                        break

                # look up the grouping parameter for the group this species/whatever is in and cut the current annotation string down to what we want
                report_string = line_split[index]
                grouped = False
                if dictionary_index < index_last_dict and splits[dictionary_index][2] in report_string:
                    try:
                        report_string = report_string[report_string.index("|"+splits[dictionary_index][2])+1:] # the beginning before the grouping parameter gets cut off
                    except ValueError: # no grouping parameter in the original annotation string ... gi is already removed, so lets keep the rest
                        #report_string = report_string[report_string.index("|")+1:]
                        pass
                    grouped = True
                elif dictionary_index == index_last_dict:
                    grouped = True # we report everything that doesn't fit into one of the split groups into our last dictionary/"other" file

                # check if only to report grouped and act accordingly
                if report_only_grouped and not grouped: # if they are the same the report string wasn't modified, i.e. grouped
                    pass # don't add read
                else: # if we report all or it has been modified/grouped
                    if report_string not in list_of_dicts[dictionary_index] and length_of_line_split == 4: # if it's not in the dictionary so far and is the only annotation for this read, add unique hit
                        list_of_dicts[dictionary_index][report_string] = [1, 1]
                    elif report_string not in list_of_dicts[dictionary_index] and length_of_line_split > 4: # if not in dict, but not unique
                        list_of_dicts[dictionary_index][report_string] = [1, 0]
                    elif report_string in list_of_dicts[dictionary_index] and length_of_line_split == 4: # in dict already and unique
                        list_of_dicts[dictionary_index][report_string][0] += 1
                        list_of_dicts[dictionary_index][report_string][1] += 1
                    else: # in dict already and not unique
                        list_of_dicts[dictionary_index][report_string][0] += 1

        # now go through the created dictionaries and sort, write them
        return_value = []
        for index, dictionary in enumerate(list_of_dicts):
            # setup csv writer
            try:
                outfile = open(outputdir+"/stats_"+splits[index][0]+"_"+splits[index][2]+".csv", "w")
                return_value += [os.path.abspath(outfile.name)]
            except IndexError: # last dictionary in list_of_dicts has no corresponding entry in splits
                outfile = open(outputdir+"/stats_other.csv", "w")
            csvwriter = csv.writer(outfile, dialect="excel", delimiter=";")

            # make an array out of the dictionary so we can sort it
            array = []
            for (key, value) in dictionary.iteritems():
                array.append([key, value[0], value[1]])

            # once we are done with creating the array, we want to sort it after the species with the most hits
            sys.stdout.write("\nSorting\n")
            sys.stdout.flush()
            if sort_by_unique_hits:
                array.sort(key=operator.itemgetter(2,1), reverse=True) # two sorts actually: result is sorted by number of unique hits and then number of overall hits; highest first
            else:
                array.sort(key=operator.itemgetter(1,2), reverse=True) # two sorts actually: result is sorted by number of hits and then number of unique hits; highest first

            # if only uniques are to be reported, omit all entries without unique reads
            if uniques_only:
                array = [entry for entry in array if entry[2] > 0]

            # and then write it to the output file
            sys.stdout.write("Writing to output file\n")
            sys.stdout.flush()
            for entry in array:
                csvwriter.writerow(entry)

        # return list with paths to the produced files
        return return_value

def quantify_annotations_in_smp_file(inputfile, outputdir, group_by="", splits=[], report_only_grouped=False, uniques_only=False, sort_by_unique_hits=False, ambiguity_threshold=0.10, gz=False):
    """
    This method creates csv files containing the stats generated from a given smp file.
    It can split the output into several files via taxonomical groups (i.e. Bacteria and Viruses)

    inputfile - string path to input smp file
    outputdir - string path to output directory
    group_by - standard grouping parameter, e.g. "species", "genus", "family", is only used if the splits parameter is left at default, default is an empty string, meaning no grouping
    splits - list containing groups to split output by - e.g. splits = [["bacteria", 2, "genus"], ["viridae", 10239, "family"]], where the number in each entry is the taxonomy id on ncbi, default is an empty list, leading to one single output file
    uniques_only - boolean defining whether to report only reads that hit only one reference, default is False
    ambiguous_removal_threshold - float defining how much better a first hit must be than the next best to be considered unambiguous and listed as such

    Return value is a list of produced files.
    """

    # setup
    gen = Utility.read_generator_smp_file(inputfile, gz)
    list_of_dicts = [{}] # we use this dictionary in case we don't have splits or some reference doesn't fit into the splits
    tax_levels = []
    if not group_by:
        group_by = "species"
    if splits:
        for split in splits:
            list_of_dicts += [{}]
            if not split[2] in tax_levels:
                tax_levels += [split[2]]
    else:
        tax_levels = [group_by]
    if not group_by and not splits:
        report_only_grouped = False
    # the dictionaries shall be of the following format
    # {"grouping string": [all hits, unique hits], "next grouping string": [all hits, unique hits]}

    sys.stdout.write("Counting hit references in file "+inputfile+"\n")
    sys.stdout.flush()
    for read in gen:
        unique = False
        if len(read[3]) == 1:
            unique = True

        scores = []
        for reference in read[3]:
            scores += [read_scoring_system(len(read[1]), reference[1], reference[2], reference[3])]
            if splits: # if splits are given, group_by is ignored
                fit_somewhere = False
                for index, split in enumerate(splits):
                    if ":"+str(split[1])+":" not in reference[0]: #only add to this splits dictionary if it really belongs there
                        continue
                    reference_tuple = give_id_for_taxonomic_level(reference[0], split[2])
                    fit_somewhere = True
                    if report_only_grouped and not reference_tuple[1] == split[2]: # save only grouped if that is desired
                        continue
                    keyname = reference_tuple[0]+" ("+reference_tuple[1]+")"
                    if unique: # unique hits add one to each value
                        try:
                            list_of_dicts[index][keyname] = [list_of_dicts[index][keyname][0]+1, list_of_dicts[index][keyname][1]+1, list_of_dicts[index][keyname][2]+1]
                        except KeyError:
                            list_of_dicts[index][keyname] = [1, 1, 1]
                    else: # non unique hits only add one to the total hits value
                        try:
                            list_of_dicts[index][keyname] = [list_of_dicts[index][keyname][0]+1, list_of_dicts[index][keyname][1], list_of_dicts[index][keyname][2]]
                        except KeyError:
                            list_of_dicts[index][keyname] = [1, 0, 0]
                if not fit_somewhere: # if entry doesn't fit into any dictionary defined by the splits, we put it in the last dictionary, our "other" dictionary
                    reference_tuple = give_id_for_taxonomic_level(reference[0], group_by)
                    keyname = reference_tuple[0]+" ("+reference_tuple[1]+")"
                    if unique: # unique hits add one to each value
                        try:
                            list_of_dicts[-1][keyname] = [list_of_dicts[-1][keyname][0]+1, list_of_dicts[-1][keyname][1]+1, list_of_dicts[-1][keyname][2]+1]
                        except KeyError:
                            list_of_dicts[-1][keyname] = [1, 1, 1]
                    else: # non unique hits only add one to the total hits value
                        try:
                            list_of_dicts[-1][keyname] = [list_of_dicts[-1][keyname][0]+1, list_of_dicts[-1][keyname][1], list_of_dicts[-1][keyname][2]]
                        except KeyError:
                            list_of_dicts[-1][keyname] = [1, 0, 0]
            else: # no splits, so we group by group_by
                reference_tuple = give_id_for_taxonomic_level(reference[0], group_by)
                keyname = reference_tuple[0]+" ("+reference_tuple[1]+")" # id (taxonomic level)
                if report_only_grouped and not reference_tuple[1] == group_by: # report only grouped if that is desired
                    continue
                if unique: # unique hits add one to each value
                    try:
                        list_of_dicts[-1][keyname] = [list_of_dicts[-1][keyname][0]+1, list_of_dicts[-1][keyname][1]+1, list_of_dicts[-1][keyname][2]+1]
                    except KeyError:
                        list_of_dicts[-1][keyname] = [1, 1, 1]
                else: # non unique hits only add one to the total hits value
                    try:
                        list_of_dicts[-1][keyname] = [list_of_dicts[-1][keyname][0]+1, list_of_dicts[-1][keyname][1], list_of_dicts[-1][keyname][2]]
                    except KeyError:
                        list_of_dicts[-1][keyname] = [1, 0, 0]

        # check if any of the non unique reads hit something pretty unambiguously
        if ambiguity_threshold and not unique:
            really_checked_levels = [] # when we actually go through the dicts on different tax levels we need to make sure one entry isnt considered more than once, because its really used tax level might be the same in both cases
            for tax_level in tax_levels: # doing this for all possible tax levels, so we don't miss any odd occurences in any odd parameter constellation
                # generate 2 lists with [score, index, organism] for highest and second highest score
                highest_index = scores.index(max(scores))
                highest = [max(scores), highest_index, read[3][highest_index][0]]
                help_scores = scores[:]
                help_scores[highest[1]] = 0.0
                while True:
                    second_highest_index = help_scores.index(max(help_scores))
                    second_highest = [max(help_scores), second_highest_index, read[3][second_highest_index][0]]
                    # if highest and second highest are on the same taxonomic branch, we look for the next best hit on another branch
                    highest_tuple = give_id_for_taxonomic_level(highest[2], tax_level)
                    second_highest_tuple = give_id_for_taxonomic_level(second_highest[2], tax_level)
                    if not highest_tuple == second_highest_tuple or second_highest[0] == 0.0:
                        break
                    else:
                        help_scores[second_highest[1]] = 0.0

                # got our pair? check if second best actually has a score, if it does and the best hit is significantly better, add an unambiguous count to that best read in every dictionary where that read is considered
                if not second_highest[0] == 0.0 and highest[0]-second_highest[0] >= (1.0+ambiguity_threshold):
                    if not highest_tuple[1] in really_checked_levels:
                        really_checked_levels += [highest_tuple[1]] # add so we don't do this exact same addition to the dicts again ...
                    else:
                        continue # if we checked this read for this tax level already we don't add it to all dicts again but just skip this time
                    keyname = highest_tuple[0]+" ("+highest_tuple[1]+")"
                    for dictionary in list_of_dicts:
                        # we need to consider all taxonomic levels that could have been used to produce the dicts, so we iterate over the splits and then try the group_by parameter as well and get every grouping parameter out of these
                        try:
                            dictionary[keyname] = [dictionary[keyname][0], dictionary[keyname][1], dictionary[keyname][2]+1]
                        except:
                            pass

    # make a list out of every dictionary, so we can sort them
    sys.stdout.write("Sorting\n")
    sys.stdout.flush()
    list_of_lists = []
    return_value = []
    for index, dictionary in enumerate(list_of_dicts):
        list_of_lists += [[]]

        #create list from current dictionary
        for (key, value) in dictionary.iteritems():
            list_of_lists[index] += [[key, value[0], value[1], value[2]]]

        # sort current list
        if sort_by_unique_hits:
            list_of_lists[index].sort(key=operator.itemgetter(2,1), reverse=True) # two sorts actually: result is sorted by number of unique hits and then number of overall hits; highest first
        else:
            list_of_lists[index].sort(key=operator.itemgetter(1,2), reverse=True) # two sorts actually: result is sorted by number of hits and then number of unique hits; highest first

        # if only uniques are to be reported, omit all entries without unique reads
        if uniques_only:
            list_of_lists[index] = [entry for entry in list_of_lists[index] if entry[2] > 0]

        # and then write it to the output file
        try:
            outfile = open(outputdir+"/stats_"+splits[index][0]+"_"+splits[index][2]+".csv", "w")
        except IndexError: # if grouped by group_by the dictionary in list_of_dicts has no corresponding entry in splits
            outfile = open(outputdir+"/stats_other.csv", "w")
        return_value += [os.path.abspath(outfile.name)]
        csvwriter = csv.writer(outfile, dialect="excel", delimiter=";")
        csvwriter.writerow(["Taxonomy", "Total Hits", "Unique Hits", "Unambiguous Hits"])

        sys.stdout.write("Writing stats to file "+outfile.name+"\n")
        sys.stdout.flush()
        for entry in list_of_lists[index]:
            csvwriter.writerow(entry)

    # return list with paths to the produced files
    return return_value

def quantify_annotations_in_smp_file_by_tree(inputfile, outputfile, sort_by="all", ambiguity_dict={"all": {"all": 0.1}}, neg_list = None, gz=False):
    # setup
    if sort_by not in ["all", "unique", "unambigous"]:
        sort_by = "all"
    gen = Utility.read_generator_smp_file(inputfile, gz)
    treeroot = Node.Node("root", "0", "root")
    counter = 0

    # going through the file
    for read in gen:
        read_obj = Read.Read(read[0], read[1], read[2])
        readroot = Node.Node("root", "0", "root")
        counter += 1
        if counter % 100 == 0:
            sys.stdout.write("Processed "+str(counter)+" reads\r")
            sys.stdout.flush()
        hits = read[3]
        unique_hit = False
        if len(hits) == 0: # should this happen? considering it anyway
            continue
        elif len(hits) == 1: # unique
            unique_hit = True

        # build tree ...
        for hit in hits:
            # add every hit to read tree
            leaf = readroot.build_read_tree(hit[0].split("||")[-1].split("|")[1:])  # 11/05/2016 adapted for new database format, still cutting off the gi
            leaf.score = read_scoring_system(len(read[1]), hit[1], hit[2], hit[3])
            leaf.set_of_reads = {read_obj}

        # prepare read tree for merging
        # determine unambiguity, remove everything from the read tree that isn't unambiguous
        Node.give_scores(readroot, ambiguity_dict) # go through the tree and pass up scores and set nodes to unambiguous if possible
        unambiguous_leaf = readroot.remove_ambiguous_nodes() # removes all the ambigous nodes, leaving us with exactly one path in the "tree"
        unambiguous_leaf.unambiguous_reads = {read_obj} # adding the read to the last unambigous node
        unambiguous_leaf.hits_specific[2] += 1
        if unambiguous_leaf.parent:
            unambiguous_leaf.parent.add_upwards_to_unambiguous()
        if unique_hit:
            unambiguous_leaf.hits_specific[1] += 1
            if unambiguous_leaf.parent:
                unambiguous_leaf.parent.add_upwards_to_unique()


        # walk the read and the sum tree down at the same time
        # for every node that exists in the sum tree, add +1 to hits_below (or hits_specific if it's a leaf)
        # if we reach a node that doesn't exist in the sum tree, add it to the sum tree
        # (cut everything above the current node in the read tree, change the parent to the last node in the sum tree,
        # add the current node in the read tree to the children in the sum tree's last node, done)
        Node.merge_read_tree_into_sum_tree(readroot, treeroot)

    sys.stdout.write("Processed "+str(counter)+" reads\n")
    sys.stdout.flush()

    # create dictionary of all nodes with names by walking the tree once
    dict_of_all_nodes = {}
    Node.walk_tree_and_create_name_dictionary(treeroot, dict_of_all_nodes)

    # create list of uninteresting taxa from file
    uninteresting_list = []
    if neg_list:
        with open(neg_list) as infile:
            for line in infile:
                uninteresting_list += [line.rstrip().split()[0]]

    # mark entries in tree if they are in the uninteresting list, downwards
    for entry in uninteresting_list:
        if entry in dict_of_all_nodes:
            for node in dict_of_all_nodes[entry]:
                Node.mark_branch_downwards(node, marker="-") # - for unintersting

    # now that we can say if a node's children are ALL marked, we mark upwards
    for entry in uninteresting_list:
        if entry in dict_of_all_nodes:
            for node in dict_of_all_nodes[entry]:
                Node.mark_branch_upwards(node, marker="-") # - for unintersting

    ## Tree to ouputfile
    # setup
    indents_dict = {}
    Node.count_indents(treeroot, indents_dict)
    columns = Node.construct_stats_file_structure(indents_dict, Node.ranks_lookup)
    columns = ["root"] + columns # root needs a column too in the output file

    outlist = []
    Node.full_output(treeroot, columns, outlist, output_nodes_without_unambiguous_reads=True)
    with open(outputfile, "w") as outfile:
        outfile.write(";".join(["marker"]+columns+["hit_name", "all_hits", "unique_hits", "unambiguous_hits", "unambiguous_hits_below", "unambiguous_hits_specific\n"]))
        for line in outlist:
            outfile.write(line)

    treedump = open(os.path.dirname(outputfile)+"/treedump.pickle", "w")
    pickle.dump(treeroot, treedump)
    return treeroot

def give_id_for_taxonomic_level(annotation_string, taxonomic_level="species"):
    # tries to give back the id on the requested taxonomic level
    # if that fails, tries to give back the id on the level one higher and so on (using ranks_lookup list)

    # prepare a list like this [["species", 123, "name_of_species"], ["genus", 456, "name_of_genus"], ...]
    # this is equivalent to split_annotation_string()
    anno_split = Utility.split_annotation_string(annotation_string)

    # go through that list and check for the id corresponding to the requested taxonomic level or the next best
    requested_index = ranks_lookup.index(taxonomic_level)
    for elem in anno_split:
        if elem[0] == "no_rank":
            continue
        this_rank_index = ranks_lookup.index(elem[0])
        if this_rank_index == requested_index: # exact match
            return (elem[2], elem[0]) # returns (id, level)
        elif this_rank_index < requested_index: # cases from which we have to select the best
            continue
        else: # this_rank_index > requested_index (since we excluded no_rank)
            return (elem[2], elem[0]) # since we want to go nearer to the tree root if we don't find an exact match, we just take the next higher level

def clean_stats_file(inputfile, outputfile, always_give_taxonomy_level=False):
    """
    This method "cleans" an input csv stats file, which means that it replaces the annotation strings by more human readable short species/genus/family/etc. names.

    inputfile - string path to input csv file
    outputfile - string path to output csv file with cleaned up identifiers
    """

    all_the_same = True
    infile = open(inputfile)
    outfile = open(outputfile, "w")
    csvreader = csv.reader(infile, dialect="excel", delimiter=";")
    csvwriter = csv.writer(outfile, dialect="excel", delimiter=";")

    # check if all entries are on the same taxonomic level (can be forced by using the stats producing methods while only reporting grouped entries etc.)
    # if they are not, we add the taxonomic level to each entry in brackets behind the entry in the next step
    if always_give_taxonomy_level:
        all_the_same = False
    else:
        try: # if input file empty this can fail ...
            level = csvreader.next()[0].split("|")[0].split(":")[0] # first entry is the annotation string of the first row in the file which is split twice to get the taxonomy level of the first item in the string
        except StopIteration: # ... with an StopIteration Error in which case we just return
            return

        for row in csvreader:
            if not row[0].split("|")[0].split(":")[0] == level:
                all_the_same = False
                break
        infile.seek(0) # reset the infile pointer position cause we go through it again now

    # write out header line and then everything else after it ...
    csvwriter.writerow(["Taxonomy", "Total Hits", "Unique Hits"])
    for row in csvreader:
        if all_the_same:
            row[0] = row[0].split("|")[0].split(":")[2]
        else:
            row[0] = row[0].split("|")[0].split(":")[2] + " (" + row[0].split("|")[0].split(":")[0] + ")"
        csvwriter.writerow(row)

def build_dict_from_smp(inputfile, uniques_only=True):
    """
    Builds a compact dictionary from an smp file that can be used to substract organisms from the stats of another smp file.

    inputfile - string path to an smp file
    uniques_only - boolean that determines whether to report reads with unique hits only

    Return a dictionary of annotation strings
    """

    infile = open(inputfile)
    result = {}

    for line in infile:
        unique = False
        line_split = line.rstrip().split()

        # get rid of the readname, sequence and qualities, leaving the first reference as line_split[0]
        for i in range(3):
            line_split.pop(0)

        if len(line_split) == 0: # no annotations
            continue
        elif len(line_split) == 4: # 1 _unique_ annotation
            report_string = line_split[0][line_split[0].index("|")+1:]
            if report_string not in result:
                result[report_string] = True
        else: # more than 1 annotation
            if uniques_only:
                continue
            else:
                length_of_line_split = len(line_split)
                for index in range(0, length_of_line_split, 4):
                    report_string = line_split[index][line_split[index].index("|")+1:]
                    if report_string not in result:
                        result[report_string] = True

    return result

def build_dict_from_stats_csv(inputfile, uniques_only=True):
    """
    Builds a compact dictionary from an existing stats file that can be used to substract organisms from the stats of another smp file.

    inputfile - string path to a csv stats file
    uniques_only - boolean that determines whether to report reads with unique hits only

    Return a dictionary of annotation strings
    """

    ###
    ### This method is not the best idea since stats file only (or mostly) contain already cropped annotation strings which can lead to cases where
    ### the removal wouldn't work correctly because entries in the dict can't correctly be paired with entries in the other stats file
    ###

    pass


def remove_negative_dict_from_stats(inputfile, outputfile, negative_dict):
    """
    Removes annotations found in the negative_dict from the stats file, for example for removal of negative controls.

    inputfile - string path to a stats csv file
    outputfile - string path to the stats csv file that shall be created after removing the negative dictionary
    negative_dict - dictionary of annotation strings to be removed from the initial stats file
    """

    infile = open(inputfile)
    outfile = open(outputfile, "w")
    csvreader = csv.reader(infile, dialect="excel", delimiter=";")
    csvwriter = csv.writer(outfile, dialect="excel", delimiter=";")

    for row in csvreader:
        found = False
        for key in negative_dict:
            if row[0] in key:
                found = True
                break
        if not found:
            csvwriter.writerow(row)


def generate_fastq_from_smp_and_stats(smp_input, stats_input, outputdir, uniques_only=True):
    """
    This method generates a fastq file for every entry in the stats file.
    The fastq file contains all the (unique) reads for that entry in the smp file.

    smp_input - string path to an smp file
    stats_input - string path to a stats csv file
    outputdir - string path to a directory where the output fastq files are saved
    uniques_only - boolean determining whether to only save unique hits
    """

    # whats the best way to solve this? going through the smp file several times, which is pretty slow
    # or produce lots of hdd i/o by switching the output file every read line ...

    smp_in = open(smp_input)
    stats_in = open(stats_input)
    csvreader = csv.reader(stats_in, dialect="excel", delimiter=";")

    # generate a list of annotation_strings first
    annotation_strings = []
    for row in csvreader:
        annotation_string = row[0].split()[0] # if we have a stats file which gives the taxonomy level too, this will be something like "Escherischia_coli (species)" from which we need to strip the taxonomy part
        if ":" or "|" in annotation_string: # if we have an uncleaned stats file we take the lowest taxonomy level ...
            temp = annotation_string[annotation_string.index(":")+1:annotation_string.index("|")]
            annotation_string = temp[temp.index(":")+1:]
        annotation_strings += [annotation_string]

    # since we append later on, we need to make sure we create new files now
    for annotation_string in annotation_strings:
        open(outputdir+"/"+annotation_string+"_reads.fastq", "w") # need to "touch" the files to be able to append later

    # go through the smp file line wise and write wanted reads to their appropriate files
    for line in smp_in:
        line_split = line.rstrip().split("\t")
        for annotation_string in annotation_strings:
            if annotation_string in line:
                if len(line_split) == 7: # unique annotation
                    outfile = open(outputdir+"/"+annotation_string+"_reads.fastq", "a")
                    outfile.write("".join(["@", line_split[0], "\n", line_split[1], "\n+\n", line_split[2], "\n"]))
                    outfile.close()
                else: # non unique annotation
                    if not uniques_only:
                        outfile = open(outputdir+"/"+annotation_string+"_reads.fastq", "a")
                        outfile.write("".join(["@", line_split[0], "\n", line_split[1], "\n+\n", line_split[2], "\n"]))
                        outfile.close()
            else: # no annotations
                continue

"""
def quantify_annotations_in_smp_file_old2(inputfile, outputfile, group_by="", report_only_grouped=False):
    # takes an input file in smp format with annotated reads and writes a table with counts of the respective annotations
    infile = open(inputfile)
    outfile = open(outputfile, "w")
    filesize = os.path.getsize(inputfile)
    bytesread = 0
    linesread = 0
    output = {}

    #go through input file line wise and build a hashmap of this form { annotationstring: [ hits, unique hits ], nextstring: [ hits, uniquehits ] }
    for line in infile:
        linesread += 1
        bytesread += len(line)+1
        if linesread % 100 == 0:
            sys.stdout.write("Read " + str(100*bytesread/filesize) + "% of the file     \r")

        line_split = line.split()

        # get rid of the readname, sequence and qualities, leaving the first reference as line_split[0]
        line_split.pop(0)
        line_split.pop(0)
        line_split.pop(0)

        # throw out the identifier that comes first in the annotation strings, so we can group stuff
        length = len(line_split)
        for index in range(0, length, 4): # go through line_split in steps of 4 iterating over the annotation strings
            temp_list = line_split[index].split("|")
            temp_list.pop(0) # get rid of the reference identifier number
            line_split[index] = "|".join(temp_list)

        # group even more if desired
        if group_by != "":
            length = len(line_split)
            for index in range(0, length, 4): # go through line_split in steps of 4 iterating over the annotation strings
                if "|"+group_by in line_split[index]:
                    line_split[index] = line_split[index][line_split[index].index("|"+group_by)+1:] # cut off everything before the grouping parameter
                else: # annotation strings without the grouping parameter
                    if not report_only_grouped: # leave them as they are if we report everything
                        continue
                    else: # delete them from the array completely if we only want the things containing the grouping parameter reported
                        line_split[index] = "" # string
                        line_split[index+1] = "" # hitlength
                        line_split[index+2] = "" # mismatches
                        line_split[index+3] = "" # clippings
            # if we removed anything we need to get rid of the empty items in the array now
            line_split = "\t".join(line_split).split()

        if len(line_split) == 0: # line without annotations
            continue
        elif len(line_split) == 4: # line with a unique annotation
            # look if annotation string in dictionary
            # if yes, add hits AND unique hits
            # if no, add whole entry
            if not output.has_key(line_split[0]):
                output[line_split[0]] = [1, 1]
            else:
                output[line_split[0]][0] += 1
                output[line_split[0]][1] += 1

        else: # line with more than one annotation
            length = len(line_split)
            for index in range(0, length, 4): # go through line_split in steps of 4 iterating over the annotation string
                if not output.has_key(line_split[index]):
                    output[line_split[index]] = [1, 0]
                else:
                    output[line_split[index]][0] += 1

    # make an array out of the dictionary so we can sort it
    array = []
    for (key, value) in output.iteritems():
        array.append([key, value[0], value[1]])

    # once we are done with creating the array, we want to sort it after the species with the most hits
    sys.stdout.write("Sorting\n")
    array.sort(key=operator.itemgetter(1,2), reverse=True) # two sorts actually: result is sorted by number of hits and then number of unique hits; highest first

    # and then write it to the output file
    sys.stdout.write("Writing to output file\n")
    for each in array:
        for index, item in enumerate(each):
            each[index] = str(item)
        outfile.write("\t".join(each)+"\n")
"""

"""
# this is unused and should stay that way! just use the above method quantify_annotations_in_smp_file and rework it if necessary
def group_annotations(inputfile, outputfile, group_by="species"):
    # takes an input file as produced by quantify_annotations_in_smp_file (might rework this to take lists instead so we can skip writing a file)
    # writes out a file that contains the annotations grouped by what is specified in the group_by parameter OR if that parameter is lacking the next bigger taxonomical rank

    infile = open(inputfile)
    outfile = open(outputfile, "w")
    output = {}

    for line in infile:

        # initialise help lookup table
        help_lookup = ranks_lookup[:] # copies the ranks lookup table by "slicing" all items into a new list
        while not (help_lookup[0] == group_by):
            help_lookup.pop(0)

        split = line.split("\t")
        split[1] = int(split[1])
        split[2] = int(split[2].strip())

        processed = False
        while not processed:
            if ("|"+help_lookup[0]) in split[0]: # if grouping parameter found, add entry to output OR add to entry in output ^^
                split[0] = split[0][split[0].index("|"+help_lookup[0])+1:] # cut off everything before the grouping parameter

                if not output.has_key(split[0]):
                    output[split[0]] = [split[1], split[2]]
                    processed = True
                else:
                    output[split[0]][0] += split[1]
                    output[split[0]][1] += split[2]
                    processed = True
            else: # grouping parameter not found, go to next one
                if len(help_lookup) > 1:
                    help_lookup.pop(0)

    res = []
    for (key, value) in output.iteritems():
        res.append([key, value[0], value[1]])

    res.sort(key=operator.itemgetter(1,2), reverse=True)

    for each in res:
        each[1] = str(each[1])
        each[2] = str(each[2])+"\n"
        outfile.write("\t".join(each))
"""

def generate_fasta_reference_for_string(string, database_list, outputfile):
    # takes a string and looks for that string in a set of fasta files
    # generates an output file that contains all the sequences with that particular string in the description
    outfile = open(outputfile, "w")
    block = ["", ""]

    for file_item in database_list:
        with open(file_item) as open_file:
            for line in open_file:
                if line[0] == ">": # evaluate last block and start putting together a new descriptor and sequence block
                    if string in block[0]:
                        block[1] += "\n"
                        outfile.write(block[0]+block[1])
                    block[0] = line # new block starts here
                    block[1] = ""
                else: # sequence line, just add them to the current block
                        block[1] += line.rstrip()

def get_reads_for_strings(string_list, inputfile, outputfile):
    # takes an smp file as input and writes all the reads that mapped to strings in a list to a new smp file
    with open(inputfile) as infile:
        with open(outputfile, "w") as outfile:
            for line in infile:
                for string in string_list:
                    if string in line:
                        outfile.write(line)
                        break

def check_stats_file_for_processed_flags(inputfile):
    # expects string path to input smp file
    # generates a list of already processed items
    # as well as a list of unprocessed items
    # and gives back those lists

    infile = open(inputfile)
    processed_list = []
    unprocessed_list = []

    for line in infile:
        if "Taxonomy" in line:
            continue
        if "processed" in line:
            processed_list.append(line.split(";")[0].split(" ")[0]) # if processed, add to array
        else: # all the unprocessed lines
            unprocessed_list.append(line.split(";")[0].split(" ")[0])

    return (processed_list, unprocessed_list)

def add_processed_flags_to_stats_file(inputfile, outputfile, processed_entries_list):
    # takes a stats file and adds back previous processed flags

    infile = open(inputfile)
    outfile = open(outputfile, "w")

    for line in infile:
        if "processed" in line:
            outfile.write(line)
        else: # no "processed" in line
            for index, each in enumerate(processed_entries_list): # check if line needs a "processed"
                if each in line:
                    outfile.write(line.rstrip()+";processed\n")
                    processed_entries_list.pop(index) # remove entry from list so next line might be processed faster
                    break # no need to go through the rest of processed entries, also skip the else clause
            else: # line doesn't need a "processed" flag
                outfile.write(line)

    return os.path.abspath(outfile.name)

"""
def basic_independent_scoring(read, hit_length, hit_clippings, hit_mismatches, mismatch_cost=2.0, clipping_cost=1.5):
    # gives scores to hits on references based on hit_length/read_length, substracts score for mismatches and clippings
    # and then multiplies with a factor between 0 and 1 based on read complexity
    read_length = float(len(read))
    normed_mismatch_cost = mismatch_cost/read_length
    normed_clipping_cost = clipping_cost/read_length
    return ((hit_length/read_length)-(hit_clippings*normed_clipping_cost)-(hit_mismatches*normed_mismatch_cost))*complexity_per_sd(read)
"""

"""
def read_scoring_system(sequence, hit_length, hit_clippings, hit_mismatches, identity_factor=1.0, complexity_factor=1.0, mismatches_factor=1.0, clippings_factor=1.0):
    # only factors now, no substractions
    read_length = float(len(sequence))

    # for the following, 1 is optimal, 0 is worst case
    identity = int(hit_length)/read_length
    clippings = 1-int(hit_clippings)/read_length
    mismatches = 1-int(hit_mismatches)/read_length
    complexity = complexity_per_sd(sequence)

    # clippings not used, cause that would be weighing them double cause they are considered in identity already
    # complexity not used because it's not a useful information when judging the hit of the same read on different references
    #return (identity_factor * identity + mismatches_factor * mismatches)/(identity_factor + mismatches_factor)
"""

def read_scoring_system(read_length, hit_length, hit_clippings, hit_mismatches):
    # returns a score calculated from coverage * identity, which equals overall percentage of hitting bases in the read
    if read_length < 36:
        return 0.0000001
    coverage = float(hit_length)/read_length # percentage of the read used in the alignment against the reference
    identity = (float(hit_length)-float(hit_mismatches))/float(hit_length) # identity of the covered region
    #overall_coverage = (int(hit_length)-int(hit_mismatches))/read_length

    #result = coverage*identity # this is equivalent to overall_coverage as (a-b)/a * a/c = (a-b)/c
    #alternative_result = (hit_length-hit_mismatches)/read_length

    result = (float(hit_length)-float(hit_mismatches))/float(read_length)

    return result

def read_scoring_system_correct(read_length, hit_length, clippings, mismatches):
    # returns a score calculated from coverage * identity, which equals overall percentage of hitting bases in the read

    if read_length < 36:
        return 0.0000001

    coverage = float(hit_length)/read_length
    identity = 1 - float(mismatches)/hit_length

    # identity of covered region = matches in covered region / length of covered region
    #


    result = identity * coverage

    return result

def complexity_per_sd(seq, k=3):
    # help vars
    seq_len = len(seq)
    kmer_no = 4**k

    # generate empty kmers dictionary for k
    kmers_dict = generate_kmers_dict(k)

    # fill dict with data from our read
    for i in range(seq_len-(k-1)):
        kmer = seq[i:i+k]
        if "N" not in kmer:
            kmers_dict[kmer] += 1
        else:
            seq_len -= 1

    # generate expectation value and "worst case" value for kmer frequency
    expectation = float(seq_len-(k-1))/kmer_no
    worst_case = float(seq_len-(k-1))

    # calculating standard deviation for each kmer to the expectation value
    sd_dict = {}
    for kmer in kmers_dict:
        sd_dict[kmer] = ((expectation-kmers_dict[kmer])**2)**(1.0/2)

    # calculating mean of these standard deviations
    average = 0
    for sd in sd_dict:
        average += sd_dict[sd]
    average /= kmer_no

    # norming over worst case scenario (result will always be between 0 and 1 now)
    normed_average = average/worst_case

    #return 1-normed_average, so that the most complex reads score nearest to 1
    return 1.0-normed_average

def generate_kmers_dict(k=3, alphabet="ACGT"):
    res = {}
    temp = ["".join(item) for item in itertools.product(alphabet, repeat=k)]
    for each in temp:
        res[each] = 0
    return res

def produce_score_file_from_smp(smp_file, sco_file):
    with open(smp_file) as infile:
        with open(sco_file, "w") as outfile:
            for line in infile:
                line_split = line.rstrip().split("\t")
                line_len = len(line_split)
                outfile.write(line_split[0])
                for i in range(3, line_len, 4):
                    outfile.write("\t".join(["", line_split[i], str(read_scoring_system(len(line_split[1]), line_split[i+1], line_split[i+2], line_split[i+3]))]))
                outfile.write("\n")

def stats_for_sample(sample_folder, stats_file, gz=False):
    """
    This is so utterly ugly, this function alone justifies a complete rewrite of the program. ^^

    :param sample_folder: folder with files for which general stats shall be created
    :param stats_file: string path to output csv file
    :return: path to stats file (input param stats_file)
    """

    # get all reads from inputfile in actual program run
    annotated_reads = 0
    unannotated_reads = 0
    background_reads = 0
    no_bg = False
    if not gz:
        try:
            iterative_subdir = [x for x in sorted(os.listdir(sample_folder)) if os.path.isdir(sample_folder+"/"+x)][0]
            annotated_reads_file = sample_folder+"/"+iterative_subdir+"/new_annotated_reads.smp"
        except:
            if os.path.isfile(sample_folder+"/annotated_reads_without_background.smp"):
                annotated_reads_file = sample_folder+"/annotated_reads_without_background.smp"
            else:
                annotated_reads_file = sample_folder+"/annotated_reads_merged.smp"
                no_bg = True
        if os.path.isfile(sample_folder+"/unannotated_reads_without_background.smp"):
            unannotated_reads_file = sample_folder+"/unannotated_reads_without_background.smp"
        else:
            unannotated_reads_file = sample_folder+"/unannotated_reads_merged.smp"
            no_bg = True
        if os.path.isfile(sample_folder+"/background_reads_merged.smp"):
            background_reads_file = sample_folder+"/background_reads_merged.smp"
        else:
            background_reads_file = None
    else:  # if gz
        try:
            iterative_subdir = [x for x in sorted(os.listdir(sample_folder)) if os.path.isdir(sample_folder+"/"+x)][0]
            annotated_reads_file = sample_folder+"/"+iterative_subdir+"/new_annotated_reads.smp.gz"
        except:
            if os.path.isfile(sample_folder+"/annotated_reads_without_background.smp.gz"):
                annotated_reads_file = sample_folder+"/annotated_reads_without_background.smp.gz"
            else:
                annotated_reads_file = sample_folder+"/annotated_reads_merged.smp.gz"
                no_bg = True
        if os.path.isfile(sample_folder+"/unannotated_reads_without_background.smp.gz"):
            unannotated_reads_file = sample_folder+"/unannotated_reads_without_background.smp.gz"
        else:
            unannotated_reads_file = sample_folder+"/unannotated_reads_merged.smp.gz"
            no_bg = True
        if os.path.isfile(sample_folder+"/background_reads_merged.smp.gz"):
            background_reads_file = sample_folder+"/background_reads_merged.smp.gz"
        else:
            background_reads_file = None

    with open(stats_file, "w") as statsfile:
        if no_bg:
            statsfile.write("No Background data, therefore could not remove background reads from mapped and unmapped read numbers.\n")
        if not gz:
            infile = open(annotated_reads_file)
        else:
            infile = gzip.open(annotated_reads_file, 'rt')
        for line in infile:
            annotated_reads += 1
        infile.close()
        if not gz:
            infile = open(unannotated_reads_file)
        else:
            infile = gzip.open(unannotated_reads_file, 'rt')
        for line in infile:
            unannotated_reads += 1
        infile.close()
        try:
            if not gz:
                infile = open(background_reads_file)
            else:
                infile = gzip.open(background_reads_file, 'rt')
            for line in infile:
                background_reads += 1
            infile.close()
        except:
            pass
        if background_reads_file is not None:
            statsfile.write("Foreground mapped reads:;"+str(annotated_reads)+"\nBackground mapped reads:;"+str(background_reads)+"\nUnmapped reads:;"+str(unannotated_reads)+"\n")
        else:
            statsfile.write("Foreground mapped reads:;"+str(annotated_reads)+"\nUnmapped reads:;"+str(unannotated_reads)+"\n")

    return stats_file

def extract_ambigous_reads(input_smp, ambigous_reads_smp, ambiguity_threshold=0.05, level="species"):
    """
    Extracts reads from smp files that are deemed ambigous as determined by ambiguity_threshold. The best hit for a read must score at least
    x % better than the second, so that the read can be classified as unambiguous. 2 output files are then produced, containing ambigous and unambigous
    reads respectively.

    :param input_smp: string path to input smp file
    :param ambigous_reads_smp: string path to output file with ambigous reads (ones that can be ignored later on)
    :param ambiguity_threshold: Minimum percental difference between the two best hits so that a read can be classified as unambigous, default = 1.10
    :param level: taxonomical level on which hits should be grouped and considered the same for quality evaluation
    :return: string path to output file
    """

    ambigous = open(ambigous_reads_smp, "w")
    gen = Utility.read_generator_smp_file(input_smp)

    for read in gen:
        if len(read[3]) == 1: # only one hit reference, so unique and unambigous!
            continue
        else: # all cases with more than 1 hit reference (and possibly no references at all, although that shouldn't occur)

            # preparing a dictionary with hit refs as keys and the scores and smp entries as values
            hits = []
            for ref in read[3]:
                hits += [[read_scoring_system(len(read[1]),ref[1], ref[2], ref[3]), ref]]

            # removing hits that don't need to be compared
            biggest = max(hits)
            for index, ref in enumerate(hits):
                if ref == biggest:
                    continue
                else:
                    # check if comparison necessary because different on chosen tax level

                    # prep
                    biggest_split = Utility.split_annotation_string(biggest[1][0])
                    this_split = Utility.split_annotation_string(ref[1][0])

                    biggest_taxid = 0
                    this_taxid = 0
                    for each in biggest_split:
                        if each[0] == level:
                            biggest_taxid = each[1]
                            break
                    for each in this_split:
                        if each[0] == level:
                            this_taxid = each[1]
                            break

                    # throw out hits that dont need to be compared
                    if biggest_taxid == this_taxid and biggest_taxid != 0: # if they are the same on the level we want join our results
                        hits[index] = "REMOVE"
                    else:
                        if biggest_taxid == 0 or this_taxid == 0:
                            if biggest_split[0] == this_split[0]:
                                hits[index] = "REMOVE"

                    while True:
                        try:
                            hits.remove("REMOVE")
                        except ValueError:
                            break

            # actually comparing and writing out
            unambigous = True
            for ref in hits:
                if ref == biggest:
                    continue
                if biggest[0] < (1.0+ambiguity_threshold) * ref[0]:
                    unambigous = False
                    break

            if not unambigous:
                ambigous.write("\t".join([read[0], read[1], read[2]]))
                for ref in read[3]:
                    ambigous.write("\t"+"\t".join([ref[0], str(ref[1]), str(ref[2]), str(ref[3])]))
                ambigous.write("\n")

    return ambigous_reads_smp

def split_uninteresting_reads_off_from_stats_file(input_stats_file, interesting_stats_file, uninteresting_stats_file, negative_list_file):
    """
    Splits stats files into interesting and uninteresting results using a negative list containing the uninteresting taxonomies

    :param input_stats_file: string path to input stats file that shall be split
    :param interesting_stats_file: string path to output stats file with interesting reads
    :param uninteresting_stats_file: string path to output stats file with uninteresting reads
    :param negative_list_file: string path to negative list file to be used
    :return: tuple of two string pathes to the interesting and uninteresting stats files
    """

    negative_list = []
    with open(negative_list_file) as infile:
        for line in infile:
            negative_list += [line.rstrip().split()[0]]
    negative_list = sorted(negative_list)

    infile = open(input_stats_file)
    header = infile.readline()
    interesting = open(interesting_stats_file, "w")
    interesting.write(header)
    uninteresting = open(uninteresting_stats_file, "w")
    uninteresting.write(header)

    for line in infile:
        done = False
        for negative_entry in negative_list:
            if line.startswith(negative_entry):
                uninteresting.write(line)
                done = True
                break
        if done:
            continue
        interesting.write(line)

def quantify_annotations_in_smp_file_new(inputfile, outputdir, sort_by="all", ambiguity_dict={"all": {"all": 0.1}},
                                         neg_list=None):
    # read line
    # construct unambiguous path
    # write out in temp file structured as path, readname(, other infos?)
    # sort temp file by path
    # construct tree from temp file
    # proceed from there like in old version

    # todo this ... unambiguous path construction via tree structure -> write out as string -> sort -> assemble

    # setup
    if sort_by not in ["all", "unique", "unambigous"]:
        sort_by = "all"
    gen = Utility.read_generator_smp_file(inputfile)
    counter = 0

    # going through the file
    for read in gen:
        read_obj = Read.Read(read[0], read[1], read[2])
        readroot = Node.Node("root", "0", "root")
        counter += 1
        if counter % 100 == 0:
            sys.stdout.write("Processed "+str(counter)+" reads\r")
            sys.stdout.flush()
        hits = read[3]
        unique_hit = False
        if len(hits) == 0: # should this happen? considering it anyway
            continue
        elif len(hits) == 1: # unique
            unique_hit = True

        # build tree ...
        for hit in hits:
            # add every hit to read tree
            leaf = readroot.build_read_tree(hit[0][hit[0].index("|")+1:].split("|")) # cutting off the gi, passing the rest of the split
            leaf.score = read_scoring_system(len(read[1]), hit[1], hit[2], hit[3])
            leaf.set_of_reads = {read_obj}

        # prepare read tree for merging
        # determine unambiguity, remove everything from the read tree that isn't unambiguous
        Node.give_scores(readroot, ambiguity_dict) # go through the tree and pass up scores and set nodes to unambiguous if possible
        unambiguous_leaf = readroot.remove_ambiguous_nodes() # removes all the ambigous nodes, leaving us with exactly one path in the "tree"
        unambiguous_leaf.unambiguous_reads = {read_obj} # adding the read to the last unambigous node
        unambiguous_leaf.hits_specific[2] += 1
        if unambiguous_leaf.parent:
            unambiguous_leaf.parent.add_upwards_to_unambiguous()
        if unique_hit:
            unambiguous_leaf.hits_specific[1] += 1
            if unambiguous_leaf.parent:
                unambiguous_leaf.parent.add_upwards_to_unique()

def taxonomic_path_to_string(root_node):
    """
    A function that deconstructs a chain of nodes into a saveable string.

    :param root_node: Node object designating the root of the path
    :return: String representation of the Node path
    """

    pass


def string_to_taxonomic_path(input_string):
    """
    A function that constructs a chain of nodes from a string in a format produced by taxonomic_path_to_string().

    :param input_string: String representation of a path of Node objects
    :return: Node object designating the root of the constructed path of Nodes
    """

    pass