class Read():
	def __init__(self, name, sequence=None, qualities=None, references=None):
		self.name = name
		self.sequence = sequence
		self.qualities = qualities
		self.references = references # should be a dictionary {name of reference: reference object / list with data describing the hit}

