import os
import subprocess
import sys
import AnnotationMethod
import Utility
import gzip

bowtie2_dir = "/usr/bin"

class Bowtie2(AnnotationMethod.AnnotationMethod):
	name = "Bowtie2"
	iterative = True

	def __init__(self, reads, reference, outputdir, tmp_dir, threads, parameters="", indexdirs=[], bg=False):
		AnnotationMethod.AnnotationMethod.__init__(self, reads, reference, outputdir, tmp_dir, threads, parameters, indexdirs, bg)

	def run(self, gz=False):
		outputdir = self.outputdir+"/"+self.name  # each method should only produce output in a sub folder named like the method
		if not os.path.isdir(outputdir):
				os.makedirs(outputdir)

		shortref = Utility.short_file_name_without_extension(self.reference)
		sys.stdout.write("Mapping reads against "+shortref+"\n")

		# look for an existing index
		index = check_for_index(self.reference, [os.path.dirname(self.reference)]+self.indexdirs)

		# if index = None, build an index, otherwise use the found index
		if not index:
			sys.stdout.write("Building Bowtie2 index\n")
			index = build_index(self.reference, outputdir)
		else:
			sys.stdout.write("Found Bowtie2 index\n")

		# bowtie2 mapping using several of the available cores
		sys.stdout.write("Starting bowtie2 mapping\n")
		if self.parameters == "":
			if bowtie2_dir:
				call = bowtie2_dir+"/bowtie2 -t --very-sensitive --no-hd -p "+str(self.threads)+" --reorder -x "+index+" -U "+self.reads+" -S "+outputdir+"/"+shortref+".sam"
			else:
				call = "bowtie2 -t --very-sensitive --no-hd -p "+str(self.threads)+" --reorder -x "+index+" -U "+self.reads+" -S "+outputdir+"/"+shortref+".sam"
			sys.stdout.write("Invoking bowtie2 with: "+call+"\n")
			subprocess.call(call, shell=True)
		else:
			if bowtie2_dir:
				call = bowtie2_dir+"/bowtie2 -t "+self.parameters.strip()+" --reorder -x "+index+" -U "+self.reads+" -S "+outputdir+"/"+shortref+".sam"
			else:
				call = "bowtie2 -t "+self.parameters.strip()+" --reorder -x "+index+" -U "+self.reads+" -S "+outputdir+"/"+shortref+".sam"
			sys.stdout.write("Invoking bowtie2 with: "+call+"\n")
			subprocess.call(call, shell=True)

		# convert sam file to own file format smp [name sequence qualities (reference hitlength hitclippings hitmismatches)xN]
		sys.stdout.write("Splitting SAM file to SMP files\n")
		if not self.bg:
			(annotated_reads, unannotated_reads) = bowtie2_sam_to_smp_splitter(outputdir+"/"+shortref+".sam", outputdir, gz=gz)
		else:
			(annotated_reads, unannotated_reads) = bowtie2_sam_to_smp_splitter(outputdir+"/"+shortref+".sam", outputdir, give_unannotated=False, gz=gz)

		# clean up
		sys.stdout.write("Deleting redundant SAM file\n")
		os.remove(outputdir+"/"+shortref+".sam")

		# return paths to the files produced
		self.output = (annotated_reads, unannotated_reads)
		return self.output

def check_for_index(reference, list_of_dirs):
	# checks a given list of directories for existance of an index for the given reference, returns path to index if
	# any exists, otherwise returns None
	references = [Utility.short_file_name_without_extension(reference), os.path.basename(reference)]
	subfolders = ["index", "."]

	for directory in list_of_dirs:
		for subfolder in subfolders:
			for ref in references:
				if os.path.isfile(directory+"/"+subfolder+"/"+ref+".1.bt2") or os.path.isfile(directory+"/"+subfolder+"/"+ref+".1.bt2l"):
					return directory+"/"+subfolder+"/"+ref

def build_index(reference, outputdir):
	devnull=open("/dev/null", "w")
	shortref = Utility.short_file_name_without_extension(reference)
	if bowtie2_dir:
		subprocess.call([bowtie2_dir+"/bowtie2-build", reference, outputdir+"/"+shortref], stdout=devnull)
	else:
		subprocess.call(["bowtie2-build", reference, outputdir+"/"+shortref], stdout=devnull)
	return os.path.abspath(outputdir)+"/"+shortref

def bowtie2_sam_to_smp_splitter(inputfile, outputdir, give_unannotated=True, gz=False):
	# convert sam file to own file format smp [name sequence qualities (reference hitlength hitclippings hitmismatches)xN]
	# expects all passed parameters to be string paths
	shortref = Utility.short_file_name_without_extension(inputfile)
	fsize = os.path.getsize(inputfile)
	bytesread = 0
	readlines = 0
	if not gz:
		annotated_reads = open(outputdir+"/"+shortref+"_annotated_reads.smp", "w")
		if give_unannotated:
			unannotated_reads = open(outputdir+"/"+shortref+"_unannotated_reads.smp", "w")
	else:
		annotated_reads = gzip.open(outputdir + "/" + shortref + "_annotated_reads.smp.gz", "wt")
		if give_unannotated:
			unannotated_reads = gzip.open(outputdir + "/" + shortref + "_unannotated_reads.smp.gz", "wt")
	readname = ""
	samfile = open(outputdir+"/"+shortref+".sam")

	for line in samfile: # process line wise
		bytesread += len(line)+1
		readlines += 1
		if readlines % 100 == 0:
			sys.stdout.write("Processed " + str(100*bytesread/fsize) + "% of the file\r")
		if line[0]=="@": # ignore header lines
			continue
		else: # process other lines
			last_id = readname
			split = line.split("\t")
			readname = split[0]
			sequence = split[9]
			qualities = split[10]
			reference = split[2]
			if int(split[1])==4: # write unmapped sequences to their respective file / no hitlength or hitmismatches etc
				if give_unannotated:
					unannotated_reads.write("\t".join([readname, sequence, qualities])+"\n")
			else: #write mapped sequences to their respective file / sequences matching more than once to the same line
				cigarlist = Utility.cigar_to_list(split[5]) # analyse cigar string for matches, mismatches and clippings
				(hitlength, hitmismatches, clippings) = Utility.evaluate_cigar_list(cigarlist)

				# search for bowtie2s counter of mismatches ...
				for item in split:
					if item.startswith("XM:i:"):
						xm = int(item[5:])
						hitlength -= xm
						hitmismatches += xm
						break

				if readname == last_id: # if same readname write NEW info to the same line / if this happens at all is dependent on the bowtie2 output, usually it only gives the "best" alignment
					annotated_reads.write("\t".join(["", reference, str(hitlength), str(clippings), str(hitmismatches)]))
				elif annotated_reads.tell() == 0: # if no sequence has been written to the output file so far
					annotated_reads.write("\t".join([readname, sequence, qualities, reference, str(hitlength), str(clippings), str(hitmismatches)]))
				else:  # every other case, i.e. sequences matching the first time
					annotated_reads.write("\n"+"\t".join([readname, sequence, qualities, reference, str(hitlength), str(clippings), str(hitmismatches)]))
	annotated_reads.write("\n")
	annotated_reads.close()
	if give_unannotated:
		unannotated_reads.close()
		return (os.path.abspath(annotated_reads.name), os.path.abspath(unannotated_reads.name))
	else:
		return (os.path.abspath(annotated_reads.name), "")
